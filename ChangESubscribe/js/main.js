$("#chooseSubCheckBox").change(function() {
    if(this.checked) {
        $("#subPanel").show();
        $("#flight").focus();
    } else {
        $("#subPanel").hide();
    }
});

function modifySms() {
    var value = $("#countryCode").val() + $("#number").val();
    $("#sms").val(value);
}

function modifyInfoType() {
    var subCheck = $("#chooseSubCheckBox:checked").length > 0;
    var flightInfoCheck = $("#flightInfoCheck:checked").length > 0;
    var airportInfoCheck = $("#airportInfoCheck:checked").length > 0;
    var localInfoCheck = $("#localInfoCheck:checked").length > 0;
    var arr = [];//"FLIGHT_INFO"
    // if (subCheck && flightInfoCheck) arr.push("FLIGHT_INFO");
    if (subCheck && airportInfoCheck) arr.push("AIRPORT_INFO");
    if (subCheck && localInfoCheck) arr.push("LOCAL_INFO");
    $("#infotype").val(arr.join(","));
}

$( "#submitButton" ).on('click', function( event ) {
    modifySms();
    modifyInfoType();
    if ($("#infotype").val().length > 0) {
        if ($("#flight").val().length == 0) {
            alert("You need to provide a flight number to use our subscription service!");
            $("#flight").focus();
        }
    }
    event.preventDefault();
    $.ajax({
      method: "POST",
      url: "http://0bb9946f.ngrok.io/ChangE/subscribe",
      dataType: 'json',
      data: { flight: $("#flight").val(), 
              sms: $("#sms").val(), 
              infotype: $("#infotype").val()}})
    .done(function() {
        alert( "Your internet connection begins in 30s!: " + msg );
    })
    .fail(function(jqXHR, textStatus, errorThrown) {
        if (jqXHR.status == 200) {
            alert( "Your internet connection begins in 30s!" );
        } else {
            $("#flight").focus();
        }
    });
});

$(function() {

});